//
//  InboxStatus.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//@interface INInboxStatus : NSObject
typedef enum{
    inboxPushSent,
    inboxPushClick,
    inboxPushDeleted
}INInboxStatus ;

extern NSString* _Nonnull const FormatInboxType_toString[];

//@end

NS_ASSUME_NONNULL_END
