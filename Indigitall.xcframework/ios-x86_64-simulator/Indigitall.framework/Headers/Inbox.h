//
//  Inbox.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInboxNotification.h"
#import "InboxStatus.h"
#import "INInboxStatus.h"
#import "InboxCallback.h"
#import "InboxBaseCallback.h"
#import "InboxNotificationsCallback.h"
#import "InboxAuthenticationCallback.h"
#import "InboxCountersCallback.h"
#import "Inbox.h"
#import "InboxGetAuthConfig.h"

NS_ASSUME_NONNULL_BEGIN

@interface Inbox : NSObject

@property (nonatomic, readonly)NSString *domainInbox;

//@property (nonatomic) INInbox *allInInbox;


@property (nonatomic) NSString *lastAccess;
@property (nonatomic) NSArray<INInboxNotification *>* notifications;
@property (nonatomic) int count;
@property (nonatomic) NSNumber *pageSize;
@property (nonatomic) NSNumber *page;
@property (nonatomic) float totalPages;

- (id) init;
- (id) initWithJson:(NSDictionary *)json;

+ (void) generateAuthTokenWithSuccess:(void(^)(NSString *token))onSuccess onError:(void(^)(INError *error))onError;

+ (void) getMessagesCountWithSuccess:(void(^)(INInboxCounters *counters))onSuccess onError:(void(^)(INError *error))onError;

+ (void) getInboxWithSuccess:(void(^)(Inbox *inbox))onSuccess onError:(void(^)(INError *error))onError;

+ (void) getInboxWithPage:(NSNumber * _Nullable)page pageSize:(NSNumber * _Nullable)pageSize dateFrom:(NSString * _Nullable)dateFrom dateTo:(NSString * _Nullable)dateTo statusList:(NSArray<NSString *> * _Nullable)statusList onSuccess:(void(^)(Inbox *inbox))onSuccess onError:(void(^)(INError *error))onError;

- (void) getNextPageWithSuccess:(void(^)(Inbox *inbox, NSArray<INInboxNotification *>* newNotifications))onSuccess onError:(void(^)(INError *error))onError;


- (void) massiveEditNotificationsWithSendingIdsList: (NSArray<NSNumber *> *)sendingIdsList status: (NSString * )status onSuccess:(void(^)(void))onSuccess onError:(void(^)(INError *error))onError;

- (void) getInfoFromNotificationWithSendingId: (NSNumber *)sendingId onSuccess:(void(^)(INInboxNotification *inboxNotification))onSuccess onError:(void(^)(INError *error))onError;

- (void) modifyStatusFromNotificationWithSendingId: (NSNumber *)sendingId status: (NSString *)status onSuccess:(void(^)(INInboxNotification *inboxNotification))onSuccess onError:(void(^)(INError *error))onError;



@end

NS_ASSUME_NONNULL_END
