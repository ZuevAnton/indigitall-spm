//
//  INInAppView.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "INError.h"
#import "InAppHandler.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppView : UIView<UIGestureRecognizerDelegate>

@property (strong, nonatomic) void (^didTouch)(INInApp *inApp,UIView *webView);
@property (strong, nonatomic) void (^onShowTimeFinished)(INInApp *inApp,UIView *webView, int showTime);
@property (strong, nonatomic) INInApp *inApp;

@property (nonatomic) WKWebView *webView;
@property (nonatomic) UIView *view;
@property (nonatomic) INInAppView *current;
@property (strong, nonatomic) NSURL *url;
@property (strong, nonatomic) NSString *noAction;
- (void)load:(UIView *)view content:(NSString *)content inApp:(INInApp *)inApp didTouch:(void(^)(INInApp *inApp,UIView *webView))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished success:(void(^)(void))success;
- (id)init;
- (void)managetouch;

@end

NS_ASSUME_NONNULL_END
