//
//  InboxCallback.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface InboxCallback : BaseCallback

typedef void(^onErrorInbox)(INError * error);
@property (readwrite, copy) onErrorInbox onError;

typedef void(^onSuccessInbox)(NSDictionary *data);
@property (readwrite, copy) onSuccessInbox onSuccess;

- (id)initWithOnSuccess: (void(^)(NSDictionary *data))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
