//
//  DeviceUtils.h
//  Indigitall
//
//  Created by indigitall on 21/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDevice.h"
#import "INDefaults.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceUtils : NSObject
+ (INDevice *) stringtoDevice: (NSString *)device;
+ (NSString *) toString: (INDevice *)device;
+ (INDevice *) compareDevices: (INDevice *)newDevice;
+ (BOOL) compareExternalApps: (NSArray<INExternalApp*>*)externalApplications toCompare:(NSArray<INExternalApp*>*)externalApplicationsTwo;
+ (NSArray *)externalAppsToJsonWithArray:(NSArray<INExternalApp *>*)externalApps;
+ (NSArray<NSNumber *>*)getAppsIdFromExternalApps: (NSArray<INExternalApp *> *) externalApplications;
@end

NS_ASSUME_NONNULL_END
