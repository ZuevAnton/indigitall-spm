//
//  EventRequestInApp.h
//  Indigitall
//
//  Created by indigitall on 6/9/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"

NS_ASSUME_NONNULL_BEGIN

@interface EventRequestInApp : BaseRequest

@property (nonatomic) NSString * deviceId;
@property (nonatomic) int inAppId;

@property (nonatomic) int lastVersionId;

- (id)initWithInAppId: (int)inAppId lastVersionId:(int)lastVersion;
- (id) eventPostInAppRequest;
@end

NS_ASSUME_NONNULL_END
