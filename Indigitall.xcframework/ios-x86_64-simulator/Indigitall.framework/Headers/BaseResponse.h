//
//  BaseResponse.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "BaseCallback.h"


NS_ASSUME_NONNULL_BEGIN

@interface BaseResponse : NSObject

typedef enum {
    object,
    array
}ResponseType;

@property (nonatomic) BaseCallback *callback;

@property (nonatomic) NSMutableDictionary *responseData;

@property (nonatomic) ResponseType responseType;
@property (nonatomic) NSArray* responseArray;
@property (nonatomic) long statusCode;
@property (nonatomic) NSString* message;

//@property (readwrite, copy) callback callback;


//- (id) init: (Result*) handler;
- (id) initWithCallback: (BaseCallback *_Nullable) callback /*type:(ResponseType*)type*/;
//- (id) initWithCallback:  (void (^)(BaseCallback *callback)) callback /*type:(ResponseType*)type*/;

- (void) processWithData: (NSData*_Nullable)data urlResponse:(NSURL*)urlResponse error:(NSError*_Nullable)error /*handler: (void (^)(BaseHandler *handler)) completion*/;


@end

NS_ASSUME_NONNULL_END
