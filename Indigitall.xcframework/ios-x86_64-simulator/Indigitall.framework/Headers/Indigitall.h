//
//  Indigitall.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "InitHandler.h"
#import "INConfig.h"
#import "INError.h"
#import "DeviceCallback.h"
#import "INPush.h"
#import "TopicCallback.h"
#import "InAppCallback.h"
#import "INPermissions.h"
#import "APIConstants.h"
#import "BaseClient.h"
#import "InboxCallback.h"
#import "InboxCountersCallback.h"
#import "InboxAuthenticationCallback.h"
#import "InboxNotificationsCallback.h"
#import "InboxStatus.h"
#import "INInboxStatus.h"
#import "Inbox.h"
#import "INCustomer.h"
#import "INCustomerField.h"
#import "INChannel.h"

NS_ASSUME_NONNULL_BEGIN

@interface Indigitall : NSObject

@property (nonatomic) NSString *domain;
@property (nonatomic) NSString *InAppdomain;

@property (nonatomic)Indigitall *current;

@property (nonatomic)NSMutableArray<NSNumber *> *notificationIdList;

@property (nonatomic) NSString *appKey;

@property (nonatomic, class, readonly)NSString *inbox_AUTH_TOKEN_EXPIRE;

//@property (class) PermissionMode permissionModelocationPermission;

+ (void) initializeWithAppKey: (NSString *)appKey;

+ (void) initializeWithAppKey: (NSString *)appKey
      onIndigitallInitialized:(nullable void(^)( NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;

+ (void) initializeWithConfig: (INConfig *)config
      onIndigitallInitialized:(nullable void(^)(NSArray<INPermissions*> *permissions, INDevice *device))onIndigitallInitialized
      onErrorInitialized:(nullable void(^)(INError *onError))onErrorInitialized;

+ (void) getDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) enableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) disableDeviceWithOnSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;

+ (void) topicsListWithOnSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) topicsSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsSubscribeWithArrayString: (NSArray<NSString *>*) topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithArrayString: (NSArray<NSString *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;
+ (void) topicsUnSubscribeWithTopic: (NSArray<INTopic *>*)topics onSuccess: (nullable void(^)(NSArray<INTopic *>*topics))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void) askNotificationPermission;
+ (void) askLocationPermission;

+ (void) setDeviceToken: (NSData *)deviceToken;
+ (void) setDeviceToken: (NSData *)deviceToken onNewUserRegistered: (nullable void(^)(INDevice *device))onNewUserRegistered;


+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier DEPRECATED_ATTRIBUTE;

+ (void) handleWithNotification:(NSDictionary *)data identifier:(NSString *_Nullable)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion DEPRECATED_ATTRIBUTE;

+ (void) handleWithResponse:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) handleWithResponse:(UNNotificationResponse *)response withCompletionHandler:(nullable void(^)(INPush *push,INPushAction *action))completion API_AVAILABLE(ios(10.0));

+ (void) performFetchWithCompletionHandler: (void(^)(UIBackgroundFetchResult handler))handler;

+ (void) receivedPushWithCompletion: (void(^)(INPush * push, int identifier))completion;


+ (void) registerDeviceForPush: (void(^)(INError *error,INDevice *device))completion;

+ (void) serviceDisabledVar: (void(^)(void))completion;
+ (void) serviceDisabledWithCompletion:(void (^)(void ))completion;

//+ (UNNotificationContent *) didReceive: (UNNotificationRequest *)request notificationContent:(UNMutableNotificationContent *)notificationContent API_AVAILABLE(ios(10.0));
//+ (UNNotificationContent *) serviceExtensionTimeWillExpire: (UNNotificationRequest *)request notificationContent:(UNMutableNotificationContent *)notificationContent API_AVAILABLE(ios(10.0));

+ (void) showInApp: (NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed didTouch:(nullable void(^)(void))didTouch;
+ (void) showInApp: (NSString *)inAppId view: (UIView *)view success:(nullable void(^)(INInApp *))success failed:(nullable void(^)(INError *error))failed didTouch:(nullable void(^)(void))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished;
+ (void) showMultipleInApp: (NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed didTouch:(nullable void(^)(INInApp *inApp,UIView *webView))didTouch;
+ (void) showMultipleInApp: (NSArray<NSString *> *)listInAppId listView: (NSArray<UIView *>*)listView success:(nullable void(^)(INInApp *inapp, UIView* view))success failed:(nullable void(^)(INError *error))failed didTouch:(nullable void(^)(INInApp *inApp,UIView *webView))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished;
+ (void) showPopup: (NSString *)popupId didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didClicked:(nullable void(^)(void))didClicked failed:(nullable void(^)(INError *error))failed;
+ (void) showPopup: (NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didClicked:(nullable void(^)(void))didClicked didDismissed:(nullable void(^)(void))didDismissed failed:(nullable void(^)(INError *error))failed;

+ (void) showPopup: (NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(nullable void(^)(void))didAppear didCancel:(nullable void(^)(void))didCancel didClicked:(nullable void(^)(void))didClicked didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished failed:(nullable void(^)(INError *error))failed;
//+ (void) updateRemoteData;

+ (void) sendCustomEvent: (NSString *)eventCustom;
+ (void) sendCustomEvent: (NSString *)eventCustom customData:(nullable NSDictionary *)customData onSuccess:(nullable void(^)(void))onSuccess onError:(nullable void(^)(INError *error))onError;

+ (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));

+ (void)serviceExtensionTimeWillExpire: (UNMutableNotificationContent *)bestAttemptContent  withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler API_AVAILABLE(ios(10.0));

+ (UNNotificationPresentationOptions) willPresentNotification API_AVAILABLE(ios(10.0));

+ (void) setExternalCode: (NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logInWithId:(NSString *) code onSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;
+ (void) logOutWithSuccess: (void(^)(INDevice *device))onSuccess onError:(void(^)(INError *error))onError;

+ (void) registerStatistics:(UNNotificationResponse *)response API_AVAILABLE(ios(10.0));
+ (void) handleWithData:(NSDictionary *)data identifier:(NSString *)identifier withCompletionHandler:(nullable void(^)(INPush *push, INPushAction *action))completion;

+ (void) getCustomerWithSuccess: (void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getCustomerInformationWithFieldNames: (NSArray<NSString *> *_Nullable)fieldNames onSuccess:(void(^)(NSArray<INCustomerField *> *customerFields))onSuccess onError:(void(^)(INError *error))onError;
+ (void) assignOrUpdateValueToCustomerFieldsWithFields: (NSDictionary *)fields onSuccess:(void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) deleteValuesFromCustomerFieldsWithFieldNames: (NSArray<NSString *> *)fieldNames onSuccess:(void(^)(INCustomer *customer))onSuccess onError:(void(^)(INError *error))onError;
+ (void) linkWithExtenalcode: (NSString *)code channel:(INChannel)channel onSuccess:(void(^)(void))onSuccess onError:(void(^)(INError *error))onError;
+ (void) unlinkWithChannel:(INChannel)channel  onSuccess:(void(^)(void))onSuccess onError:(void(^)(INError *error))onError;
+ (void) getLocation:(void(^)(NSString *latitude, NSString *longitude))location;
@end

NS_ASSUME_NONNULL_END
