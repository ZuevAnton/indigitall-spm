//
//  InAppShowOnce.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InAppShowOnce : NSObject

@property (nonatomic, class, readonly) NSString *INAPP_ID;
@property (nonatomic, class, readonly) NSString *SCHEME;
@property (nonatomic, class, readonly) NSString *CREATED_DATE;
@property (nonatomic, class, readonly) NSString *EXPIRED_DATE;
@property (nonatomic, class, readonly) NSString *NUMBER_OF_SHOWED;
@property (nonatomic, class, readonly) NSString *NUMBER_OF_CLICKS;


@property (nonatomic) int inAppId;
@property (nonatomic) NSString *scheme;
@property (nonatomic) NSString *createdDate;
@property (nonatomic) NSString *expiredDate;
@property (nonatomic) int timesShowed;
@property (nonatomic) int timesClicked;

- (id)initWithInAppId: (int)inAppId scheme:(NSString *)scheme createdDate:(NSString *)createdDate expiredDate:(NSString *)expiredDate numberOfShowed: (int)numberOfShowed timesClicked: (int) timesClicked;

- (id)initWithNSString: (NSString *) jsonString;

- (NSDictionary *)toJson;

- (NSString *) toString;

@end

NS_ASSUME_NONNULL_END
