//
//  INErrorUtils.h
//  Indigitall
//
//  Created by indigitall on 29/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "INError.h"
#import "INErrorCode.h"
NS_ASSUME_NONNULL_BEGIN

@interface INErrorUtils : NSObject

@property (nonatomic, class, readonly) NSString *INBOX_GET_ERROR_MESSAGE;
@property (nonatomic, class, readonly) NSString *INBOX_GET_PAGE_ERROR_MESSAGE;
@property (nonatomic, class, readonly) NSString *INAPP_POPUP_ERROR_MESSAGE;
@property (nonatomic, class, readonly) NSString *INAPP_ERROR_MESSAGE;
@property (nonatomic, class, readonly) NSString *INAPP_ERROR_CONTENT_MESSAGE;
@property (nonatomic, class, readonly) NSString *INAPP_EXPIRED;
@property (nonatomic, class, readonly) NSString *INAPP_TIMES_CLICKED;
@property (nonatomic, class, readonly) NSString *PUSH_ERROR_INITIALIZATION;
@property (nonatomic, class, readonly) NSString *INBOX_EXTERNAL_ID_NO_REGISTERED;
@property (nonatomic, class, readonly) NSString *INBOX_NO_MESSAGES;
@property (nonatomic, class, readonly) NSString *INBOX_IS_REQUESTING_PAGE;
@property (nonatomic, class, readonly) NSString *INBOX_EMPTY;
@property (nonatomic, class, readonly) NSString *CUSTOMER_NO_EXTERNAL_CODE;
@property (nonatomic, class, readonly) NSString *CUSTOMER_GENERAL_ERROR;

+ (INError *) apiError: (int)statusCode message:(NSString *)message;

+ (INError *) pushError: (int)statusCode message: (NSString *)message;


+ (INError *) inappError: (int)statusCode message: (NSString *)message;

+ (INError *) inboxError: (int)statusCode message: (NSString *)message;
+ (INError *) customerError: (int)statusCode message: (NSString *)message;

@end

NS_ASSUME_NONNULL_END
