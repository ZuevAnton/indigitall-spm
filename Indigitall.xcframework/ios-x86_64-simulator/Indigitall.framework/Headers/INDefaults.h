//
//  INDefaults.h
//  indigitall+objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "KeyChainService.h"
#import "INExternalApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INDefaults : NSObject

@property (nonatomic)NSString *appKey;
@property (nonatomic)NSString *deviceKey;
@property (nonatomic)NSString *pushTokenKey;
@property (nonatomic)NSString *productNameKey ;
@property (nonatomic)NSString *productVersionKey;
@property (nonatomic)NSString *pushPermissionAskedKey;
@property (nonatomic)NSString *pushPermissionStatusSentKey;
@property (nonatomic)NSString *pushPermissionModeKey;
@property (nonatomic)NSString *locationPermissionAskedKey;
@property (nonatomic)NSString *locationPermissionModeKey;
@property (nonatomic)NSString *locationPermissionStatusSentKey;
@property (nonatomic)NSString *visitEventTimestampKey;
@property (nonatomic)NSString *enabledKey;
@property (nonatomic)NSString *configEnabledKey;
@property (nonatomic)NSString *configServiceSyncTimeKey;
@property (nonatomic)NSString *configLastCallTimestampKey;
@property (nonatomic)NSString *configMaintenanceTimeStartKey;
@property (nonatomic)NSString *configMaintenanceTimeEndKey;
@property (nonatomic)NSString *configLocationEnabledKey;
@property (nonatomic)NSString *domain;
@property (nonatomic)NSString *domainInApp;
@property (nonatomic)NSString *domainInbox;
@property (nonatomic)NSString *externalId;
@property (nonatomic)NSString *inboxAuthMode;
@property (nonatomic)NSString *authToken;
@property (nonatomic)NSString *inboxLastAccess;
@property (nonatomic)NSString *networkUpdateMinutes;
@property (nonatomic)NSString *networkEventsEnabled;
@property (nonatomic)NSString *networkSSID;
@property (nonatomic)NSString *wifiFilterEnabled;
@property (nonatomic)NSString *inAppEnabled;
@property (nonatomic)NSString *forceSimulatorToken;
@property (nonatomic)NSString *networkUpdateMinutesKey;
@property (nonatomic)NSString *networkEventsEnabledKey;
@property (nonatomic)NSString *networkSSIDKey;
@property (nonatomic)NSString *wifiFilterEnabledKey;
@property (nonatomic)NSString *secureSendingEnabledKey;
@property (nonatomic)NSString *secureSendingAppPublicKeyKey;
@property (nonatomic)NSString *secureKeyKey;
@property (nonatomic)NSString *secureDataKey;
@property (nonatomic)NSString *inAppShowOnceKey;
@property (nonatomic)NSString *sdkVersionKey;
@property (nonatomic)NSString *inAppTimesClickedKey;
@property (nonatomic)NSString *deviceJsonKey;
@property (nonatomic)NSString *externalAppsKey;
@property (nonatomic)NSString *externalAppsForPostMethodKey;

//- (id) init;
+ (void) setAppKey:(NSString *) appKey;
+ (NSString *) getAppKey;
+ (void) setDeviceId:(NSString *) deviceId;
+ (NSString *) getDeviceId;
+ (void) setPushToken:(NSString *) pushToken;
+ (NSString *) getPushToken;
+ (void) setProductName:(NSString *) productName;
+ (NSString *) getProductName;
+ (void) setProductVersion:(NSString *) productVersion;
+ (NSString *) getProductVersion;

+ (BOOL) getPushPermissionAsked;
+ (void) setPushPermissionAsked:(BOOL) pushPermissionAsked;
+ (int) getPushPermissionStatus;
+ (void) setPushPermissionStatus:(int) pushPermissionStatus;
+ (int) getPushPermissionMode;
+ (void) setPushPermissionMode:(int ) pushPermissionMode;


+ (BOOL) getLocationPermissionAsked;
+ (void) setLocationPermissionAsked:(BOOL) locationPermissionAsked;
+ (int) getLocationPermissionStatus;
+ (void) setLocationPermissionStatus:(int ) locationPermissionStatus;
+ (int) getLocationPermissionMode;
+ (void) setLocationPermissionMode:(int ) locationPermissionMode;


+ (int64_t) getEventVisitTimestamp;
+ (void) setEventVisitTimestamp:(int64_t) eventVisitTimestamp;

+ (void) setEnabled:(BOOL) enabled;
+ (BOOL) getEnabled;

+ (void) setConfigEnabled:(BOOL) enabled;
+ (BOOL) getConfigEnabled;

+ (void) setConfigServiceSyncTime:(int) config;
+ (int) getConfigServiceSyncTime;

+ (void) setConfigLastCallTimestamp:(int64_t) config;
+ (int64_t) getConfigLastCallTimestamp;

+ (void) setConfigMaintenanceTimeStart:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeStart;

+ (void) setConfigMaintenanceTimeEnd:(NSString *) config;
+ (NSString *) getConfigMaintenanceTimeEnd;

+ (void) setConfigLocationEnabled:(BOOL) config;
+ (BOOL) getConfigLocationEnabled;

+ (void) setDomain:(NSString *)domain;
+ (NSString *) getDomain;
+ (void) setDomainInApp:(NSString *)domainInApp;
+ (NSString *) getDomainInApp;
+ (void) setDomainInbox:(NSString *)domainInbox ;
+ (NSString *) getDomainInbox;

+ (void) setExternalId:(NSString *)externalId;
+ (NSString *) getExternalId;

+ (void) setAuthToken:(NSString *)authToken;
+ (NSString *) getAuthToken;

+ (void) setInboxLastAccess:(NSString *)lastAccess;
+ (NSString *) getInboxLastAccess;

+ (void) setInboxAuthMode:(NSString *)inboxAuthMode;
+ (NSString *) getInboxAuthMode;

+ (void) setNetworkUpdateMinutes:(int)networkEventsEnabled;
+ (int) getNetworkUpdateMinutes;

+ (void) setNetworkEventsEnabled:(BOOL)networkEventsEnabled;
+ (BOOL) getNetworkEventsEnabled;

+ (void) setNetworkSSID:(NSString *)networkSSID;
+ (NSString *) getNetworkSSID;

+ (void) setWifiFilterEnabled:(BOOL)wifiFilterEnabled ;
+ (BOOL) getWifiFilterEnabled;
+ (void) setInAppEnabled :(BOOL)inAppEnabled ;
+ (BOOL) getInAppEnabled;

+ (void) setForceSimulatorToken :(BOOL)forceSimulatorToken ;
+ (BOOL) getForceSimulatorToken;
+ (void) setSecureSendingEnabled:(BOOL)secureSendingEnabled;
+ (BOOL) getSecureSendingEnabled;

+ (void) setSecureSendingAppPublicKey: (NSString *)secureSendingAppPublicKey;
+ (NSString *) getSecureSendingAppPublicKey;

+ (void) setSecureKey: (NSString *)setSecureKey;
+ (NSString *) getSecureKey;

+ (void) setSecureData: (NSString *)secureData;
+ (NSString *) getSecureData;


+ (void) setInAppShowOnce:(NSString *)showOnce;
+ (NSString *) getInAppShowOnce;

+ (void) setSDKVersion:(NSString *)sdkVersion;
+ (NSString *) getSDKVersion;

+ (void) setInAppTimesClicked:(NSString *)timesClicked;
+ (NSString *) getInAppTimesClicked;
+ (void) setDeviceJson:(NSString *)deviceJson;
+ (NSString *) getDeviceJson;

+ (void) setExternalApps:(NSArray<INExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INExternalApp *> *) getExternalApps;

+ (void) setExternalAppsForPostMethod:(NSArray<INExternalApp *> * _Nullable )externalApps ;
+ (NSArray<INExternalApp *> *) getExternalAppsForPostMethod;
@end

NS_ASSUME_NONNULL_END
