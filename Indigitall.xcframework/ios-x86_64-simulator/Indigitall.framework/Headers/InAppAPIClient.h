//
//  InAppAPIClient.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "INLog.h"
#import "BaseClient.h"
#import "RequestInApp.h"
#import "EventRequestInApp.h"
#import "InAppCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface InAppAPIClient : BaseClient
+ (void) loadInApp: (RequestInApp *)request completion:(InAppCallback *)callback;

+ (void) postEventRequest: (EventRequestInApp *)request completion:(BaseCallback *)callback;
@end

NS_ASSUME_NONNULL_END
