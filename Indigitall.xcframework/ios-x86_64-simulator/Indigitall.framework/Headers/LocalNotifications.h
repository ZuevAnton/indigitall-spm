//
//  LocalNotifications.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <UserNotifications/UserNotifications.h>

NS_ASSUME_NONNULL_BEGIN

@interface LocalNotifications : NSObject
- (void) sendNotificationForLocation: (CLLocation *)location;
@end

NS_ASSUME_NONNULL_END
