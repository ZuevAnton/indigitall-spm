//
//  INExternalApp.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import "APIConstants.h"

NS_ASSUME_NONNULL_BEGIN

@interface INExternalApp : NSObject

// MARK: - Properties
/// The external app indigitall ID
@property (nonatomic) int iNExternalAppId;
/// A string containing the name of the app
@property (nonatomic) NSString *name;
/// A string containing the external app url scheme
@property (nonatomic) NSString *iosCode;

- (id) init: (NSDictionary *)json;
- (int) getINExternalAppId;
- (NSMutableDictionary *)toJson;
@end

NS_ASSUME_NONNULL_END
