//
//  DateUtils.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DateUtils : NSObject

+ (NSDate *)getDateNow;
+ (NSDate *)getDate: (NSString *)dateString;
+ (NSDate *)getDateExpired: (NSDateComponents *)dateComponents;
+ (NSString *)getDateString: (NSDate *)date;

@end

NS_ASSUME_NONNULL_END
