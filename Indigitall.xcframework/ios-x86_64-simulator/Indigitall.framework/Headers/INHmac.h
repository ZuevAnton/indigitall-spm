//
//  INHmac.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

NS_ASSUME_NONNULL_BEGIN

@interface INHmac : NSObject
- (NSString *)hmacsha256:(NSString *)data secret:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
