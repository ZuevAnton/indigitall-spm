//
//  InboxConstants.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface InboxConstants : NSObject
@property (nonatomic, class, readonly) NSString *domain;

@property (nonatomic, class, readonly) NSString *INBOX_AUTH_TOKEN_EXPIRE;
@end

NS_ASSUME_NONNULL_END
