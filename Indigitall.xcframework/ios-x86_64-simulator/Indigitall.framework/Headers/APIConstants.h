//
//  APIConstants.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface APIConstants : NSObject

//@property (nonatomic, class, readonly) NSString * domain;
@property (nonatomic, class, readonly) NSString * pathApplication;
@property (nonatomic, class, readonly) NSString * pathApplicationAll;
@property (nonatomic, class, readonly) NSString * pathDevice;
@property (nonatomic, class, readonly) NSString * pathTopic;
@property (nonatomic, class, readonly) NSString * pathEvent;
@property (nonatomic, class, readonly) NSString * pathEventPush;
@property (nonatomic, class, readonly) NSString * pathEventVisit;
@property (nonatomic, class, readonly) NSString * pathEventPermission;
@property (nonatomic, class, readonly) NSString * pathEventLocation;


@property (nonatomic, class, readonly) NSString * params_pushToken;
@property (nonatomic, class, readonly) NSString * params_externalApps;




@property (nonatomic, class, readonly) NSString * application_externalApps;
@property (nonatomic, class, readonly) NSString * application_externalApps_id;
@property (nonatomic, class, readonly) NSString * application_externalApps_name;
@property (nonatomic, class, readonly) NSString * application_externalApps_iosCode;
@property (nonatomic, class, readonly) NSString * application_configuration;
@property (nonatomic, class, readonly) NSString * application_configuration_enabled;
@property (nonatomic, class, readonly) NSString * application_configuration_serviceSyncTime;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow_end;
@property (nonatomic, class, readonly) NSString * application_configuration_maintenanceWindow_start;
@property (nonatomic, class, readonly) NSString * application_configuration_locationEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_networkEventsEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_networkUpdateMinutes;
@property (nonatomic, class, readonly) NSString * application_configuration_secureSendingEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_secureSendingAppPublicKey;
@property (nonatomic, class, readonly) NSString * device_deviceID;
@property (nonatomic, class, readonly) NSString * device_enabled;
@property (nonatomic, class, readonly) NSString * topics_topics;
@property (nonatomic, class, readonly) NSString * topics_name;
@property (nonatomic, class, readonly) NSString * topics_code;
@property (nonatomic, class, readonly) NSString * topics_parentCode;
@property (nonatomic, class, readonly) NSString * topics_visible;
@property (nonatomic, class, readonly) NSString * topics_subscribed;
@property (nonatomic, class, readonly) NSString * application_configuration_inAppEnabled;
@property (nonatomic, class, readonly) NSString * application_configuration_inboxAuthMode;


@end

NS_ASSUME_NONNULL_END
