//
//  INInAppProperties.h
//  Indigitall
//
//  Created by indigitall on 16/9/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppLayout.h"
#import "INInAppAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppProperties : NSObject

@property (nonatomic) NSURL *InAppContentUrl;
@property (nonatomic) int numberOfShows;
@property (nonatomic) int numberOfClicks;
@property (nonatomic) INInAppLayout *layout;
@property (nonatomic) INInAppAction *action;
@property (nonatomic) int showTime;

- (id) initWithJson: (NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
