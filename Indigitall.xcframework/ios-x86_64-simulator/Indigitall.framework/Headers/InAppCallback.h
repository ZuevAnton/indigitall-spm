//
//  InAppCallback.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCallback.h"
#import "INInApp.h"
#import "INInAppView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InAppCallback : BaseCallback

@property (nonatomic) INInApp *inApp;
@property (nonatomic) NSString *content;

//@property (nonatomic) void (^didload)(INInAppView * inAppView);
@property (nonatomic) void (^failed)(INError * error);

@property (nonatomic) void (^didAppear)(void);
@property (nonatomic) void (^didCancel)(void);
@property (nonatomic) void (^didClicked)(void);

typedef void(^onErrorinApp)(INError * error);
@property (readwrite, copy) onErrorinApp onError;

typedef void(^onSuccesinApp)(INInApp * inApp);
@property (readwrite, copy) onSuccesinApp onSuccess;

- (id)initWithOnSuccess: (void(^)(INInApp * inApp))onSuccess onError:(void(^)(INError * error))onError;

- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;

@end

NS_ASSUME_NONNULL_END
