//
//  INApplication.h
//  Indigitall
//
//  Created by indigitall on 03/10/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INExternalApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface INApplication : NSObject

@property (nonatomic) NSMutableDictionary *configuration;
@property (nonatomic) NSMutableArray<INExternalApp *> *externalApps;

- (id)init: (NSDictionary *) json;
@end

NS_ASSUME_NONNULL_END
