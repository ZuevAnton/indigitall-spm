//
//  INError.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INErrorCode.h"

NS_ASSUME_NONNULL_BEGIN

@interface INError : NSObject{
}
//@property (readwrite, nonatomic) int statusCode;
//@property (readwrite, nonatomic) NSString* message;

//- (id)init: (NSError * _Nullable )error;
//- (id)initWithCode: (int)code  message: (NSString *)message;
//- (id)initWithNil;

//@property (readwrite, nonatomic) int errorId;
@property (readwrite, nonatomic) INErrorCode errorCode;
@property (readwrite, nonatomic) NSString * message;

//- (id)initWithId:(int)errorId errorCode:(INErrorCode)errorCode errorMessage:(NSString *)message;
- (id)initWithErrorCode:(INErrorCode)errorCode errorMessage:(NSString *)message;
@end

NS_ASSUME_NONNULL_END
