//
//  INPushConstants.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INPushConstants : NSObject
@property (nonatomic, class, readonly) NSString * pushId;
@property (nonatomic, class, readonly) NSString * appKey;
@property (nonatomic, class, readonly) NSString * title;
@property (nonatomic, class, readonly) NSString * body;
@property (nonatomic, class, readonly) NSString * image;
@property (nonatomic, class, readonly) NSString * gif;
@property (nonatomic, class, readonly) NSString * data;
@property (nonatomic, class, readonly) NSString * action;
@property (nonatomic, class, readonly) NSString * type;
@property (nonatomic, class, readonly) NSString * typeApp;
@property (nonatomic, class, readonly) NSString * typeURL;
@property (nonatomic, class, readonly) NSString * typeCall;
@property (nonatomic, class, readonly) NSString * typeMarket;
@property (nonatomic, class, readonly) NSString * typeShare;
@property (nonatomic, class, readonly) NSString * typeWallet;
@property (nonatomic, class, readonly) NSString * destroy;
@property (nonatomic, class, readonly) NSString * topics;
@property (nonatomic, class, readonly) NSString * buttons;
@property (nonatomic, class, readonly) NSString * buttonLabel;
@property (nonatomic, class, readonly) NSString * securedData;
@property (nonatomic, class, readonly) NSString * securedKey;
@property (nonatomic, class, readonly) NSString * PUSH_ID;
@property (nonatomic, class, readonly) NSString * SENDING_ID;
@property (nonatomic, class, readonly) NSString * CAMPAIGN_ID;
@property (nonatomic, class, readonly) NSString * JOURNEY_STATE_ID;
@end

NS_ASSUME_NONNULL_END
