//
//  INPermissionStatus.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

//@interface INPermissionStatus : NSObject
//@property (nonatomic) NSString *enabled;
//@property (nonatomic) NSString *denied;
//@property (nonatomic) NSString *notDeterminate;
//
//@end

typedef enum {
    notDeterminated,
    accepted,
    denied
} INPermissionStatus;

extern NSString* _Nonnull const FormatStateType_toString[];

NS_ASSUME_NONNULL_END
