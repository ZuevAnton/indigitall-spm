//
//  BaseClient.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RequestDevice.h"
#import "BaseResponse.h"
#import "INLog.h"

NS_ASSUME_NONNULL_BEGIN

@interface BaseClient : NSObject

//@property (nonatomic) Result *result;

@property (nonatomic) bool serviceEnabled;

+ (void) get: (NSString *)endpoint request:(BaseRequest *) request baseResponse: (BaseResponse *)response;
+ (void) post: (NSString *)endpoint request:(RequestDevice *) request baseResponse: (BaseResponse *)response;
+ (void) put: (NSString *)endpoint request:(RequestDevice *) request baseResponse: (BaseResponse *)response;
+ (void) delete: (NSString *)endpoint request:(BaseRequest *) request baseResponse: (BaseResponse *)response;
+ (void) manage: (NSMutableURLRequest *)request body:(nullable NSDictionary *)bodyJson baseResponse: (BaseResponse *)response;


@end

NS_ASSUME_NONNULL_END
