//
//  INDevice.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INExternalApp.h"

NS_ASSUME_NONNULL_BEGIN
@class DeviceHandler;
@interface INDevice : NSObject

@property (nonatomic, readonly) NSString * JSON_APP_VERSION;
@property (nonatomic, readonly) NSString * JSON_CARRIER;
@property (nonatomic, readonly) NSString * JSON_DEVICE_BRAND;
@property (nonatomic, readonly) NSString * JSON_DEVICE_ID;
@property (nonatomic, readonly) NSString * JSON_DEVICE_MODEL;
@property (nonatomic, readonly) NSString * JSON_DEVICE_TYPE;
@property (nonatomic, readonly) NSString * JSON_ENABLED;
@property (nonatomic, readonly) NSString * JSON_EXTERNAL_APPLICATIONS;
@property (nonatomic, readonly) NSString * JSON_EXTERNAL_CODE;
@property (nonatomic, readonly) NSString * JSON_LOCALE;
@property (nonatomic, readonly) NSString * JSON_OS_NAME;
@property (nonatomic, readonly) NSString * JSON_OS_VERSION;
@property (nonatomic, readonly) NSString * JSON_PLATFORM;
@property (nonatomic, readonly) NSString * JSON_PRODUCT_NAME;
@property (nonatomic, readonly) NSString * JSON_PRODUCT_VERSION;
@property (nonatomic, readonly) NSString * JSON_PUSH_TOKEN;
@property (nonatomic, readonly) NSString * JSON_SDK_VERSION;
@property (nonatomic, readonly) NSString * JSON_TIME_OFFSET;
@property (nonatomic, readonly) NSString * JSON_TIME_ZONE;

/// Indigital deviceID
@property (nonatomic, nullable) NSString *deviceID;
/// Array of external apps
@property (nonatomic, nullable) NSArray<INExternalApp *> *externalApplications;
//@property (nonatomic) INExternalApp *externalApplications;

// MARK: - Local properties
/// Push notification token
@property (nonatomic) NSString *pushToken;
/// Current platform
@property (nonatomic) NSString *platform;
/// SKD versión
@property (nonatomic) NSString *sdkVersion;
/// Third party product name
@property (nonatomic) NSString *productName;
/// Third party product version
@property (nonatomic) NSString *productVersion;    /// Operative system name
@property (nonatomic) NSString *osName;
/// Operative system versión
@property (nonatomic) NSString *osVersion;
/// Device brand
@property (nonatomic) NSString *deviceBrand;
/// Device model
@property (nonatomic) NSString *deviceModel;
/// Carrier
@property (nonatomic) NSString *carrier;
/// Device type (mobile or tablet)
@property (nonatomic) NSString *deviceType;
/// Current app version
@property (nonatomic) NSString *appVersion;
/// Device preferred languages
@property (nonatomic) NSString *locale;
/// Current time zone
@property (nonatomic) NSString *timeZone;
/// Current time offset in hours
@property (nonatomic) long timeOffset;
/// Current external code
@property (nonatomic, nullable) NSString *externalCode;

@property (nonatomic) NSNumber *enabled;
@property (nonatomic) BOOL configEnabled;

- (id)init;
- (id)initWithNoData;
- (id)initWithData: (NSMutableDictionary * _Nullable) data;

- (BOOL) getConfigEnabled;

//+ (INExternalApp *) setExternalApps:(NSMutableDictionary *)externalApps;

- (void) update: (NSDictionary *)json;
//- (void) setup: (NSString *)token;
//- (void) updateRemoteData;

//- (void) getDevice/*: (DeviceHandler *)completion*/;
//- (void) setDeviceStatus: (BOOL)enabled /*completion:(DeviceHandler*) completion*/;
- (id) saveExternalCode: (NSString *_Nullable)code /*completion: (DeviceHandler*) completion*/;
//
//- (void) manage: (BaseResponse *)response completion:(void(^)(DeviceHandler *handler))completion;
- (NSMutableDictionary *)toJsonForPutMethod: (BOOL)isPutMethod;

@end

NS_ASSUME_NONNULL_END
