//
//  ResponseDevice.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponse.h"
#import "INDevice.h"
NS_ASSUME_NONNULL_BEGIN

@interface ResponseDevice : BaseResponse
    @property (nonatomic) INDevice *device;
    @property (nonatomic) BOOL isPutMethod;
    
- (id) initWithCallback: (BaseCallback *_Nullable) callback isPutMethod:(BOOL)isPutMethod;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_END
