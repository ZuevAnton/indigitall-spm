//
//  INInAppAction.h
//  Indigitall
//
//  Created by indigitall on 04/03/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INBaseAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInAppAction : INBaseAction

- (id) init: (NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
