//
//  CustomerFieldCallback.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCallback.h"
#import "INCustomerField.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomerFieldCallback : BaseCallback

@property (nonatomic) NSMutableArray<INCustomerField *> *customerField;

typedef void(^onErrorCustomerField)(INError * error);
@property (readwrite, copy) onErrorCustomerField onError;

typedef void(^onSuccessCustomerField)(NSArray<INCustomerField *> *customerField);
@property (readwrite, copy) onSuccessCustomerField onSuccess;

- (id)initWithOnSuccess: (void(^)(NSArray<INCustomerField *> *))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;



@end

NS_ASSUME_NONNULL_END
