//
//  INInAppViewController.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INError.h"
#import "InAppHandler.h"
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppViewController : UIViewController<UIGestureRecognizerDelegate, WKUIDelegate, WKNavigationDelegate>

@property (nonatomic) void (^didAppear)(void);
@property (nonatomic) void (^didTouch)(void);
@property (nonatomic) void (^didDismissed)(void);
@property (nonatomic) void (^didCancel)(void);
@property (nonatomic) void (^onShowTimeFinished)(INInApp *inApp, int showTime);

@property (nonatomic) WKWebView *webView;
@property (nonatomic) INInApp *inapp;

- (id) initWithContent:(NSString *)content inApp: (INInApp *)inApp closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled failed:(void(^)(INError *error))failed;

@end

NS_ASSUME_NONNULL_END
