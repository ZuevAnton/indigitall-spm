//
//  INInAppLayout.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface INInAppLayout : NSObject

@property (nonatomic, class, readonly) NSString * BODER_RADIUS;
@property (nonatomic) int borderRadius;

- (id) initWithJson: (NSDictionary *)json;

@end

NS_ASSUME_NONNULL_END
