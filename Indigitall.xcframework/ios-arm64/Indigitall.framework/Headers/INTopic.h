//
//  INTopic.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIConstants.h"
NS_ASSUME_NONNULL_BEGIN

@interface INTopic : NSObject

// MARK: - Properties
/// A string containing the topics's code
@property (nonatomic) NSString *code;
/// A string containing the topics's name
@property (nonatomic) NSString *name;
/// A string containing the button's parent code
@property (nonatomic) NSString *parentCode;
/// A bool that inidicates if the action is visible
@property (nonatomic) BOOL visible;
/// A bool that inidicates if the user is subscribed
@property (nonatomic) BOOL subscribed;

- (id) init: (NSMutableDictionary *)json;

@end

NS_ASSUME_NONNULL_END
