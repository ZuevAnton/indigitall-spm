//
//  InboxAuthMode.h
//  Indigitall
//
//  Created by indigitall on 05/05/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
typedef enum{
    NONE,
    DEFAULT,
    WEBHOOK
}INInboxAuthMode ;

extern NSString* _Nonnull const FormatInboxAuthMode_toString[];

NS_ASSUME_NONNULL_END
