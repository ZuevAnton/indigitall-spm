//
//  PermissionHandler.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIKit/UIKit.h"
#import <UserNotifications/UserNotifications.h>
#import <AuthenticationServices/AuthenticationServices.h>

NS_ASSUME_NONNULL_BEGIN

@interface PermissionHandler : NSObject
typedef enum{
    PermissionNotDeterminated,
    PermissionAllowed,
    PermissionDisabled
}PermissionStatus;


//@property (nonatomic) PermissionStatus *permissionState;
@property (nonatomic) int permissions;

//- (id) initWithPermissionStatus:(PermissionStatus *)permission;
- (void) registerForPushNotification;
//+ (void) set:(NSData *)deviceToken;
- (int) permissionStatus;
- (void) askUserForNotificationPermission;
- (void) sendAskEvent;
- (void) sendEvent: (int)status;


@end

NS_ASSUME_NONNULL_END
