//
//  INInApp.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppProperties.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInApp : NSObject

@property (nonatomic) int inAppId;
@property (nonatomic) int InAppWidth;
@property (nonatomic) int InAppHeight;
@property (nonatomic) int lastVersionId;
@property (nonatomic) NSString *code;
@property (nonatomic) INInAppProperties *properties;
@property (nonatomic) int showOnce;
@property (nonatomic) NSString * expiredDate;
@property (nonatomic) NSString * creationDate;

- (id) init;

- (id) initWithJson: (NSDictionary *)json;

@end


NS_ASSUME_NONNULL_END
