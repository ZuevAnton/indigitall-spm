//
//  Channel.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum {
    PUSH,
    INAPP,
    CHAT
} INChannel;

extern NSString* _Nonnull const FormatChannelType_toString[];


NS_ASSUME_NONNULL_END
