//
//  InboxNotification.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INPush.h"
#import "InboxStatus.h"
#import "InboxCategory.h"

NS_ASSUME_NONNULL_BEGIN

@interface INInboxNotification : NSObject


@property (nonatomic) NSString *idInboxNotification;
@property (nonatomic) NSString *externalId;
@property (nonatomic) NSString *sentAt;
@property (nonatomic) InboxStatus *inboxStatus;
@property (nonatomic) NSNumber *sendingId;
@property (nonatomic) NSNumber *campaignId;
@property (nonatomic) INPush *message;
@property (nonatomic) NSString *replacements;
@property (nonatomic) Boolean read;
@property (nonatomic) InboxCategory *category;

- (id) init;
- (id) initWithJson: (NSDictionary *)json;
- (id) initWithJson: (NSDictionary *)json lastAccess: (NSString *)lastAccess;
@end

NS_ASSUME_NONNULL_END
