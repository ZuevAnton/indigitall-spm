//
//  ResponseApp.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseResponse.h"
#import "INExternalApp.h"
#import "ApplicationCallback.h"
NS_ASSUME_NONNULL_BEGIN

@interface ResponseApp : BaseResponse
@property (nonatomic) NSArray<INExternalApp *> *externalApps;
@property (nonatomic) NSDictionary *remoteConfiguration;

- (id) initWithCallback: (ApplicationCallback *_Nullable) callback;
- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error /*handler:(void (^)(BaseHandler *handler)) completion*/;

@end

NS_ASSUME_NONNULL_END
