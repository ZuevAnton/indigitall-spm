//
//  InAppUtils.h
//  Indigitall
//
//  Created by indigitall on 12/3/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "INInApp.h"

NS_ASSUME_NONNULL_BEGIN

@interface InAppUtils : NSObject
+ (BOOL) isWasShown: (INInApp *)inApp;
+ (void) updateInAppExpired;
+ (BOOL) isShouldBeShown: (INInApp *)inApp;
+ (void) addInAppNewClick:(INInApp *)inApp;
@end

NS_ASSUME_NONNULL_END
