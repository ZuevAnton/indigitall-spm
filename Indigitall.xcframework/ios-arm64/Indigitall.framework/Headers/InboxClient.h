//
//  InboxClient.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InboxAuthenticationRequest.h"
#import "InboxCountersRequest.h"
#import "InboxPushRequest.h"
#import "BaseClient.h"

NS_ASSUME_NONNULL_BEGIN

@interface InboxClient : BaseClient


+ (void) getInbox: (InboxPushRequest *) request completion: (BaseCallback *)callback;
+ (void) putInboxPush: (InboxPushRequest *) request completion: (BaseCallback *)callback;
+ (void) getInboxPushWithSendingId: (InboxPushRequest *) request completion: (BaseCallback *)callback;
+ (void) putInboxPushwithSendingId: (InboxPushRequest *) request completion: (BaseCallback *)callback;
+ (void) getInboxCounter: (InboxCountersRequest *) request completion: (BaseCallback *)callback;
+ (void) postInboxAuth: (InboxAuthenticationRequest *) request completion: (BaseCallback *)callback;

@end

NS_ASSUME_NONNULL_END
