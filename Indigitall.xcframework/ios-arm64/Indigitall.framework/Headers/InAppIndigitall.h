//
//  InAppIndigitall.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INInAppView.h"
#import "INInAppViewController.h"
#import "InAppCallback.h"
#import "Indigitall.h"

#import <UIKit/UIKit.h>


NS_ASSUME_NONNULL_BEGIN

@interface InAppIndigitall : NSObject
@property (nonatomic, readonly)NSString *domainInApp;

@property (nonatomic, readonly)Indigitall *indigitallView;

+ (void) showInAppWithInAppId: (NSString *)inAppId view: (UIView *)view success:(void(^)(INInApp *))success failed:(void(^)(INError *error))failed didTouch:(void(^)(void))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished;

+ (void) showMultipleInAppWithListInAppId: (NSArray<NSString *>*) listInAppId listView: (NSArray<UIView *>*)listView success:(void(^)(INInApp *inapp, UIView* view))success failed:(void(^)(INError *error))failed didTouch:(void(^)(INInApp *inApp,UIView *webView))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished;

+ (void) showPopupWithPopupId: (NSString *)popupId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear: (void(^)(void))didAppear didCancel:(void(^)(void))didCancel didClicked:(void(^)(void))didClicked didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(void(^)(INInApp *inApp, int showTime))onShowTimeFinished failed:(void(^)(INError * error))failed;

@end

NS_ASSUME_NONNULL_END
