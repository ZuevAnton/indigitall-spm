//
//  RequestInApp.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseRequest.h"
#import "INDevice.h"

NS_ASSUME_NONNULL_BEGIN

@interface RequestInApp : BaseRequest

@property (nonatomic) INDevice *device;
@property (nonatomic) NSString *inAppId;

-(id) initWithInAppId: (NSString *)inAppId device: (INDevice *)device;
@end


NS_ASSUME_NONNULL_END
