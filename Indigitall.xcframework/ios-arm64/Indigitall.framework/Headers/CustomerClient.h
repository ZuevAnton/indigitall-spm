//
//  CustomerClient.h
//  Indigitall
//
//  Created by indigitall on 14/6/21.
//  Copyright © 2021 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerRequest.h"
#import "BaseClient.h"

NS_ASSUME_NONNULL_BEGIN

@interface CustomerClient : BaseClient

+ (void) getCustomer: (CustomerRequest *)request completion: (BaseCallback *)callback;
+ (void) getCustomerField: (CustomerRequest *)request completion: (BaseCallback *)callback;
+ (void) putCustomerField: (CustomerRequest *)request completion: (BaseCallback *)callback;
+ (void) deleteCustomerField: (CustomerRequest *)request completion: (BaseCallback *)callback;
+ (void) postCustomerLink: (CustomerRequest *)request completion: (BaseCallback *)callback;
+ (void) deleteCustomerLink: (CustomerRequest *)request completion: (BaseCallback *)callback;

@end

NS_ASSUME_NONNULL_END
