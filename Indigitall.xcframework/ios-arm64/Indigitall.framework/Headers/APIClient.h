//
//  APIClient.h
//  indigitall-objc
//
//  Created by indigitall on 03/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseClient.h"
//#import "BaseCallback.h"
#import "RequestDevice.h"
#import "RequestApp.h"
#import "RequestEvent.h"
#import "ApplicationCallback.h"

NS_ASSUME_NONNULL_BEGIN

@interface APIClient :BaseClient

+ (void) loadConfiguration: (RequestApp *)request completion:(ApplicationCallback *)callback;
+ (void) postDevice: (RequestDevice *)request completion:(BaseCallback *)callback;
+ (void) putDevice: (int)status request:(RequestDevice *)request completion:(BaseCallback *)callback onDeviceNotUpdated: (void(^)(INDevice * device))onDeviceNotUpdated;
+ (void) getDevice: (RequestDevice *)request completion:(BaseCallback *)callback;
+ (void) getTopics: (RequestDevice *)request completion:(BaseCallback *)callback;
+ (void) subscribeTopics: (RequestDevice *)request completion:(BaseCallback *)callback;
+ (void) unsubscribeTopics: (RequestDevice *)request completion:(BaseCallback *)callback;
+ (void) eventPush: (RequestEvent *)request completion:(BaseCallback *)callback;
+ (void) eventVisit: (RequestEvent *)request;
+ (void) eventPermission: (RequestEvent *)request completion:(BaseCallback *)callback;
+ (void) eventLocation: (RequestEvent *)request completion:(BaseCallback *)callback;
+ (void) eventCustomSend: (RequestEvent *)request completion:(BaseCallback *)callback;
+ (void) eventNetwork: (RequestEvent *)request completion:(BaseCallback *)callback;

@end

NS_ASSUME_NONNULL_END
