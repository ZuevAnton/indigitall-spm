//
//  InboxNotificationsCallback.h
//  Indigitall
//
//  Created by indigitall on 07/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseCallback.h"
#import "INInboxNotification.h"

NS_ASSUME_NONNULL_BEGIN

@interface InboxNotificationsCallback : BaseCallback

@property (nonatomic) INInboxNotification *inboxNotification;

typedef void(^onErrorInboxNotification)(INError * error);
@property (readwrite, copy) onErrorInboxNotification onError;

typedef void(^onSuccessInboxNotification)(INInboxNotification * inboxNotification);
@property (readwrite, copy) onSuccessInboxNotification onSuccess;

- (id)initWithOnSuccess: (void(^)(INInboxNotification *inboxNotification))onSuccess onError:(void(^)(INError * error))onError;
- (void) proccessDataWithStatusCode:(int)statusCode message:(NSString *)message data:(NSMutableDictionary *_Nullable)data;


@end

NS_ASSUME_NONNULL_END
