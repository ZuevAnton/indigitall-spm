//
//  NotificationHandler.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INPush.h"
#import "INLog.h"

#import <PassKit/PassKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationHandler : NSNotificationCenter
@property (nonatomic) int sendNotificationID;
@property (nonatomic) bool showAlert;
@property (nonatomic) INLog * log;

- (id) init;
+ (INPush *)getPushObject:(NSDictionary *)data;
//- (void)handleNotificationData: (INPush *)push identifier: (NSString *_Nullable)identifier;
- (void)handleNotificationData: (long)pushId completion:(void(^)(bool success))completion;
+ (void)handleWithAction:(INPushAction *)action;
+ (void) eventStatistics:(int)buttonclicked push:(INPush*)push;
//- (void)handleWithAction: (INPushAction *)action buttonClicked:(int)buttonclicked push:(INPush*)push;
//-(void) openApp: (NSString *) string;
//-(void) openURL: (NSString *)string;
//-(void) call: (NSString *)string;
//-(void) market: (NSString *)string;
//-(void) share: (NSString *)string;
//-(void) openWallet: (NSString *)string;

@end

NS_ASSUME_NONNULL_END
