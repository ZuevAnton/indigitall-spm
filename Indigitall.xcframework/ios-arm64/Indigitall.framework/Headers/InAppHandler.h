//
//  InAppHandler.h
//  indigitall-objc
//
//  Created by indigitall on 09/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

#import "INInApp.h"
#import "INError.h"
#import "INInAppViewController.h"
#import "UIApplicationExtension.h"

NS_ASSUME_NONNULL_BEGIN
@class INInAppView;
@interface InAppHandler : NSObject

@property (nonatomic) int width;
@property (nonatomic) int height;

@property (nonatomic,strong) NSURL *inAppURL;

- (id)init;

- (void) showInApp: (NSString *)inAppId view: (UIView *)view success:(void(^)(INInApp *))success failed:(void(^)(INError *error))failed didTouch:(void(^)(void))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp, int showTime))onShowTimeFinished;

- (void) showMultipleInApp:  (NSArray<NSString *>*) listInAppId listView: (NSArray<UIView *>*)listView success:(void(^)(INInApp *inApp, UIView *view))success failed:(void(^)(INError *error))failed didTouch:(void(^)(INInApp *inApp,UIView *webView))didTouch onShowTimeFinished:(nullable void(^)(INInApp *inApp,UIView *webView, int showtime))onShowTimeFinished;

- (void) showPopup:(NSString *)popUpId closeIcon:(nullable UIButton *)closeIcon closeIconDisabled:(Boolean)closeIconDisabled didAppear:(void(^)(void))didAppear didCancel:(void(^)(void))didCancel didClicked:(void(^)(void))didClicked didDismissed:(nullable void(^)(void))didDismissed onShowTimeFinished:(void(^)(INInApp *inApp, int showTime))onShowTimeFinished failed:(void(^)(INError *error))failed;

- (void)getInApp:(NSString *)inAppId success:(void(^)(NSString * content,INInApp *inApp))success errorLoad:(void(^)(INError *error))errorLoad;

- (void) loadContent:(INInApp *)inApp success:(void(^)(NSString * content,INInApp *inApp))success failed:(void(^)(INError *error))failed;

- (void) open: (NSURL *)url;


@end

NS_ASSUME_NONNULL_END

