//
//  InitHandler.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDevice.h"
#import "INPermissions.h"

NS_ASSUME_NONNULL_BEGIN

@interface InitHandler : NSObject
- (void) onErrorInitialized: (NSString *)error;
- (void) onNewUserRegistered: (INDevice *)device;
- (void) onIndigitallInitialized: (NSArray<INPermissions *>*)permissions device: (INDevice *)device;
- (void) onPushPermissionChangeOldState: (bool)oldState newState:(bool)newState;
- (void) onLocationPermissionChangeOldState: (bool)oldState newState:(bool)newState;
@end

NS_ASSUME_NONNULL_END
