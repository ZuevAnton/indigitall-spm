//
//  InboxGetAuthConfig.h
//  Indigitall
//
//  Created by indigitall on 23/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN


@protocol GetAuthConfig

- (NSDictionary *)getAuthConfig;

@end

@interface InboxGetAuthConfig : NSObject{
    id delegate;
}

- (NSDictionary *) getAuthConfig;

@end

NS_ASSUME_NONNULL_END
