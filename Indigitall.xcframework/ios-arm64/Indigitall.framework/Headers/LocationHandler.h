//
//  LocationHandler.h
//  indigitall-objc
//
//  Created by indigitall on 04/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


NS_ASSUME_NONNULL_BEGIN

@interface LocationHandler : CLLocationManager<CLLocationManagerDelegate>
@property (nonatomic)LocationHandler * _Nullable shared;

typedef enum{
    notWasDeterminated,
    allowed,
    notAllowed
}LocationStatus;

//@property (nonatomic) CLLocation * _Nullable lastLocation;
@property (nonatomic) int locationStatus;
@property (nonatomic) bool locationAllowed;
@property (nonatomic) CLLocationManager * _Nullable locationManager;

- (id _Nullable ) init;
- (void) startMonitoringWithAsk: (BOOL) ask;
- (void) configLocationHasChanged;
- (void) askPermission;
- (void) askPermissionBySystem;

- (void) startMonitoring;
- (void) sendAskEvent;
- (void) sendEvent:(int )status;

- (void) locationManager:(CLLocationManager *_Nullable)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status;
- (void) locationManager:(CLLocationManager *_Nullable)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations;
- (void) fetchSSIDInfo;


@end

NS_ASSUME_NONNULL_END
