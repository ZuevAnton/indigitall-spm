//
//  CryptoExportImportManager.h
//  Indigitall
//
//  Created by indigitall on 06/08/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CryptoExportImportManager : NSObject

@property (nonatomic)NSArray *kCryptoExportImportManagerSecp256r1header;
@property (nonatomic)NSArray *kCryptoExportImportManagerSecp384r1header;
@property (nonatomic)NSArray *kCryptoExportImportManagerSecp521r1header;

@property (nonatomic)NSArray *kCryptoExportImportManagerRSAOIDHeader;
+ (NSString*) exportPublicKeyToPEM:(NSData *)rawPublicKeyBytes keyType:(NSString*)keyType keySize: (int)keySize ;

@end

NS_ASSUME_NONNULL_END
