//
//  INConfig.h
//  indigitall-objc
//
//  Created by indigitall on 02/09/2019.
//  Copyright © 2019 indigitall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "INDevice.h"
#import "INDefaults.h"
#import "INInboxAuthMode.h"

NS_ASSUME_NONNULL_BEGIN

//@class INDevice;
@interface INConfig : NSObject

@property (nonatomic) INConfig *current;
@property (nonatomic) INDevice *device;
@property (nonatomic) INDefaults *indefaults;

// MARK: - PermissionMode

/// Tells to the SDK if it has to ask for a particular permission or not
///
/// - manual: the integrator handle the permission request
/// - helped: the integrator tells when to ask but is the SDK who asks for it
/// - automatic: the sdk handle when ask for the permission

//typedef NS_ENUM(NSInteger, PermissionMode) {
//   /// The integrator handle the permission request
//     manual,
//     /// The sdk handle when ask for the permission
//     automatic,
//     /// For notifications - Send one notification before ask for permission
//     provisional
//};

typedef enum permissionMode {
    manual=0,
    automatic=1,
    provisional=2
} PermissionMode;

- (BOOL) configEnabled;
// MARK: - Properties
/// Public app identifier from indigitall
@property (nonatomic) NSString * appKey;
/// The push permission mode - automatic by default
@property (nonatomic) PermissionMode pushPermissionMode;
/// The location permission mode - manual by default
@property (nonatomic) PermissionMode locationPermissionMode;
/// The minimun background fetch interval - 3600 miliseconds by default
@property (nonatomic) NSTimeInterval minimumBackgroundFetchInterval;
/// Host of the indigitall backend/enviroment
@property (nonatomic,readwrite) NSString * domain;
/// Host of the indigitall backend/environment InApp Api
@property (nonatomic) NSString * domainInApp;
@property (nonatomic) NSString * domainInbox;
/// Enable debug mode in order to see more info in logs - false by default
@property (nonatomic) BOOL debugMode;
//@property (nonatomic) NSString * productVersion;
//@property (nonatomic) NSString* productName;
@property (nonatomic) BOOL wifiFilterEnabled;
@property (nonatomic) BOOL networkEventsEnabled;
@property (nonatomic) int networkUpdateMinutes;
@property (nonatomic) BOOL inAppEnabled;
@property (nonatomic) INInboxAuthMode inboxAuthMode;
/// Enable if is used to compile a simulator to get a fake token to work property
@property (nonatomic) BOOL forceSimulatorToken;

@property (nonatomic) BOOL secureSendingEnabled;
@property (nonatomic) NSString *secureSendingAppPublicKey;

+ (id) current;
- (id) init;
- (id) initWithAppKey:(NSString *) appKey;
- (id) initWithJSON: (NSDictionary *)json;
- (void) setProductVersion:(NSString *) productVersion;
- (void) setProductName: (NSString *) productName;
- (void) enabled:(BOOL)enabled;

//- (void) updateWithConfig:(INConfig *)config;
- (void) updateConfig;






@end

NS_ASSUME_NONNULL_END
