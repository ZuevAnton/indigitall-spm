//
//  Inboxresponse.h
//  Indigitall
//
//  Created by indigitall on 06/04/2020.
//  Copyright © 2020 Indigital. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "BaseResponse.h"

NS_ASSUME_NONNULL_BEGIN

@interface InboxResponse : BaseResponse

- (id)initWithCallback:(BaseCallback *_Nullable)callback;

- (void) processWithData:(NSData *_Nullable)data urlResponse:(NSURL *)urlResponse error:(NSError *_Nullable)error;

@end

NS_ASSUME_NONNULL_END
